package com.galasti.awsbeanstalkdemo.endpoint;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;

@RestController
@RequestMapping(path = "/v1/aws")
public class MainController {

    @GetMapping(value = "test")
    public String test() throws Exception {
        final InetAddress ip = InetAddress.getLocalHost();
        final String hostname = ip.getHostName();
        return String.format("Testing AWS Cluster Service at host: %s", hostname);
    }
}
